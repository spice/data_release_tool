import subprocess
import json
import warnings
import pypandoc
import numpy as np
import pandas as pd
import requests
import sys
from pathlib import Path
from urllib.error import HTTPError
from tempfile import TemporaryDirectory
import jinja2
from astropy.io import fits
try:
    from rich import print
except ImportError:
    print('Warning: please install the Python `rich` module for colored output.')
try:
    from . import config
except ImportError:
    print('[red]Error:[/] Could not import config, please copy'
          'config-template.py to config.py and adapt it to your local setup')
    sys.exit(2)


def get_file_relative_path(cat_row):
    """
    Get file relative path for a given catalog entry

    Parameters
    ----------
    cat_row: pandas.Series
        SPICE catalog entry

    Return
    ------
    pathlib.PosixPath
        File path, relative to the "fits" directory of the file archive:
        leveln/yyyy/mm/dd
    """
    file_path = cat_row.FILE_PATH
    if file_path.startswith("fits/"):
        file_path = file_path[5:]
    return Path(file_path)


def get_file_fullname(cat_row):
    """
    Get file full name (with path) for a given catalog entry

    Parameters
    ----------
    cat_row: pandas.Series
        SPICE catalog entry

    Return
    ------
    pathlib.PosixPath
        File path (directory and file name)
    """
    return Path(config.data_path) / "fits" / get_file_relative_path(cat_row) / cat_row.FILENAME


def get_file_headers(cat_row):
    """
    Get some FITS headers and metadata not included in catalog,
    by accessing and reading file

    Parameters
    ----------
    cat_row: pandas.Series
        SPICE catalog entry

    Return
    ------
    dict
        File size and FITS headers: processing steps, CRVAL[12], CRVAL3 for
        each window
    """
    max_proc_steps = 20
    file_full_name = get_file_fullname(cat_row)
    header = {
        "proc_steps": list(),
        "windows": dict(),
        "file_size": file_full_name.stat().st_size
    }
    if cat_row.LEVEL != "L2":
        return header
    with fits.open(file_full_name) as hdu_list:
        # Get processing steps and parameters from first (or any) window HDU
        # TODO Are these always the same in all windows?
        hdu = hdu_list[0]
        for proc_step in range(1, max_proc_steps + 1):
            step_info = dict()
            for key in ["PRPROC", "PRPVER", "PRPARA", "PRLIB", "PRREF"]:
                value = hdu.header.get(key + str(proc_step), None)
                if value is not None:
                    step_info[key] = value
            if step_info:  # non-empty
                header["proc_steps"].append(step_info)
        # Get other global keywords not in database
        for key in []:  # Add keyword names here as needed
            value = hdu.header.get(key, None)
            if value is not None:
                header[key] = value
        # Get window-specific information
        for hdu in hdu_list:
            name = hdu.name
            if name == "VARIABLE_KEYWORDS":
                continue  # we take only HDUs corresponding to windows
            win_info = dict()
            for key in ["CRVAL3"]:
                value = hdu.header.get(key)
                if value is not None:
                    win_info[key] = value
            header["windows"][name] = win_info
    return header


def dir_size(directory):
    """
    Directory size

    Parameters
    ----------
    directory: str
        Directory

    Return
    ------
    int
        Directory size in B
    """
    du_args = [
        'du',
        '-skL',
        directory,
    ]
    output = subprocess.run(du_args, capture_output=True, check=True)
    return int(output.stdout.decode().split('\t')[0]) * 1024


def available_remote_space(server, directory):
    """
    Available space on volume containing remote directory

    Parameters
    ----------
    server: str
        Server
    directory: str
        Directory on server

    Return
    ------
    int
        Available space on remote volume, in B
    """
    df_args = [
        'ssh',
        server,
        'df',
        '-k',
        '--output=avail',
        directory,
    ]
    output = subprocess.run(df_args, capture_output=True, check=True)
    return int(output.stdout.decode().split('\n')[1]) * 1024


def human_size(size):
    """
    Human-readable representation of large sizes with SI unit prefixes

    Parameters
    ----------
    size: float
        Number of units

    Return
    ------
    string
        Human-readable representation of number
    """
    if size == 0:
        return '0'
    ls = int(np.log10(size) / 3)
    try:
        prefix = ['', 'k', 'M', 'G', 'T', 'P', 'E'][ls]
    except IndexError:
        raise RuntimeError('Value too small or too large for list of implemented prefixes')
    size /= 1000. ** ls
    return f'{size:.3g} {prefix}'


def has_enough_space(local_dir, server, remote_dir):
    """
    Check space on remote server for transferring files

    Parameters
    ----------
    local_dir: str
        Local directory containing files to transfer
    server: str
        Server name
    remote_dir
        Directory on server

    Return
    ------
    bool
        True if volume for remote directory has enough space to transfer
        local directory (with some margin)
    """
    files_volume = dir_size(local_dir)
    available_space = available_remote_space(server, remote_dir)
    print(f'Will transfer {human_size(files_volume)}B, remote space {human_size(available_space)}B')
    if files_volume > .8 * available_space:
        print('[yellow]This is possibly not enough![/]')
        return False
    else:
        return True


def get_dark(proc_steps):
    """
    Get dark (SPICE Obs ID or file name) from FITS headers for processing steps.

    This does not include dark file used for on-board dark processing (this is DARKSPID).

    This works for darks identified by their file names, but not anymore
    (as in older files) for darks identified by their Obs ID.

    Parameters
    ----------
    proc_steps: dict
        FITS header (as extracted by `get_file_headers()`),
        including processing steps

    Return
    ------
    int or str or None
        Reference to dark (if any)
    """
    for proc_step in proc_steps:
        if "dark_offset_correction" in proc_step["PRPROC"]:
            params = proc_step.get("PRPARA", "=").split()
            for param in params:
                key, value = param.split("=")
                if key in ["dark_spiobsid", "SPIOBSID_of_dark"]:
                    return int(value)
                if key in ["dark"]:
                    return value
            params = proc_step.get("PRREF", "=")
            if params.startswith("solo_L1_spice"):
                return params
            list_params = params.split(",")
            for param in list_params:
                if param.startswith("dark ="):
                    return param.split(" = ")[1]
            print(f"[yellow]Warning:[/] parameter for dark correction not found: {proc_step}")
    return None


class SpiceCat(pd.DataFrame):
    def __init__(self, data_frame=None, read_cat=False):
        if read_cat:
            super().__init__(SpiceCat.read_uio_cat())
        else:  # also when data_frame is None
            super().__init__(data_frame)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.header_cache_file = Path(config.header_cache)
            self.header_cache = dict()

    @staticmethod
    def read_uio_cat():
        """
        Read local copy of UiO CSV table of SPICE FITS files catalog
        http://astro-sdc-db.uio.no/vol/spice/fits/spice_catalog.csv

        Return
        ------
        pandas.DataFrame
            Table

        Example queries that can be done on the result:

        * `df[(df.LEVEL == "L2") & (df["DATE-BEG"] >= "2020-11-17") &
            (df["DATE-BEG"] < "2020-11-18") & (df.XPOSURE > 60.)]`
        * `df[(df.LEVEL == "L2") &
            (df.STUDYDES == "Standard dark for cruise phase")]`
        """
        cat_file = Path(config.data_path) / "fits" / "spice_catalog.csv"
        if not cat_file.exists():
            print(f'[red]Error:[/] Catalog file not available at {cat_file.as_posix()}')
            sys.exit(3)
        date_columns = ['DATE-BEG','DATE', 'TIMAQUTC']
        df = pd.read_csv(cat_file, low_memory=False)
        for date_column in date_columns:
            df.loc[df[date_column] == "MISSING", date_column] = "NaT"
            df[date_column] = pd.to_datetime(df[date_column], format="ISO8601")
        return df

    def _read_header_cache(self):
        """
        Read header cache
        """
        if self.header_cache_file.exists():
            print('Reading header cache file')
            with open(self.header_cache_file, 'r') as header_cache_file_handle:
                self.header_cache = json.load(header_cache_file_handle)
        else:
            print('[yellow]Warning:[/] header cache file does not exist, starting from empty cache')

    def _save_header_cache(self):
        """
        Save header cache
        """
        with open(self.header_cache_file, 'w') as header_cache_file_handle:
            print('Writing header cache file')
            json.dump(self.header_cache, header_cache_file_handle)

    def _update_header_cache(self):
        """
        Update header cache so that it includes relevant headers from all files in current catalog
        """
        print('Updating header cache')
        self.apply(self._update_header_cache_one_file, axis=1)
        self._save_header_cache()

    def _update_header_cache_one_file(self, cat_row):
        if cat_row.FILENAME not in self.header_cache.keys():
            self.header_cache[cat_row.FILENAME] = get_file_headers(cat_row)

    def add_headers(self):
        """
        Add columns for additional headers not in SPICE catalog, but read
        in L2 FITS files

        These columns are `proc_steps` and `windows`, encoded as JSON,
        as well as the dark observation ID or file.
        """
        columns = ["proc_steps", "windows", "file_size"]
        tmp_col = "headers_dict"
        self._read_header_cache()
        self._update_header_cache()
        self[tmp_col] = self.apply(lambda cat_row: self.header_cache[cat_row.FILENAME], axis=1)
        for column in columns:
            self[column] = self.apply(
                lambda row: json.dumps(row[tmp_col][column]),
                axis=1
            )
        self["dark"] = self.apply(
            lambda row: get_dark(row[tmp_col]["proc_steps"]),
            axis=1
        )
        # TODO: add reference to flat-field and other files needed
        # for calibration
        self.drop(tmp_col, axis=1, inplace=True)

    def find_file_dependencies(self, l2_files):
        """
        Find the files used to build the given L2 files

        Parameters
        ----------
        l2_files: pandas.DataFrame
            Catalog entries for some L2 files

        Return
        ------
        pandas.DataFrame
            Catalog entries for the files used to build the given L2 files
        """
        # L1 parent files
        l1_files_names = list(l2_files.PARENT)
        l1_files = self[self.apply(lambda row: row.FILENAME in l1_files_names,
                                   axis=1)]
        # darks
        l2_dark = set(l2_files.dark_ground)
        dark_files = self[self.apply(
            lambda row: (row.LEVEL == "L1") & (row.FILENAME in l2_darkspid),
            axis=1
        )]
        return SpiceCat(pd.concat([l1_files, dark_files]))

    def list_file_names(self):
        """
        Get list of all file names

        Return
        ------
        pandas.Series
            File name (directory and file name), as strings

        To get a string of space-separated names, use `" ".join()` on the
        result.
        """
        return self.apply(lambda row: get_file_fullname(row).as_posix(),
                          axis=1)

    def check_file_presence(self):
        """
        Check file presence on disk

        Return
        ------
        pandas.Series
            True or False for each entry: True if file exists
        """
        return self.apply(lambda row: get_file_fullname(row).exists(), axis=1)

    def get_previous_versions(self, frozen_new):
        """
        Get previous version (already in the release) of each file

        Parameters
        ----------
        frozen_new: pandas.DataFrame
            New versions of the frozen files

        Return
        ------
        frozen_old: pandas.DataFrame
            Old versions of the frozen files, that are already in the release
        """
        release_catalog_url = f'{config.release_base_url}release-{config.release_tag}/catalog.csv'
        try:
            current_release_catalog = pd.read_csv(release_catalog_url, low_memory=False)
        except HTTPError:
            raise RuntimeError(f'Could not find previous release catalog at {release_catalog_url}')
        def find_previous_version(cat_row):  # see SoarSender._is_reprocessed()
            same_level = (current_release_catalog.LEVEL == cat_row.LEVEL)
            same_spiobsid = (current_release_catalog.SPIOBSID == cat_row.SPIOBSID)
            same_rasterno = (current_release_catalog.RASTERNO == cat_row.RASTERNO)
            other_version = (current_release_catalog.VERSION != cat_row.VERSION)
            all_conditions = (same_level & same_spiobsid & same_rasterno & other_version)
            assert all_conditions.sum() == 1
            return current_release_catalog[all_conditions].iloc[0]
        frozen_old = frozen_new.apply(find_previous_version, axis=1)
        return frozen_old

    def total_size(self):
        """
        Return
        ------
        int
            Total size of files
        """
        return self.file_size.apply(int).sum()

    def index_limit_size(self, max_total_size):
        """
        Find index such that total file size up to index is not greater
        than given maximum total size

        Parameters
        ----------
        max_total_size: int
            Maximum total size (bytes)

        Return
        ------
        int
            Index of range upper boundary
        """
        a = 0
        b = len(self)
        if self.total_size() < max_total_size:
            return b
        while a + 1 < b:
            c = (a + b) // 2
            if self.iloc[:c].file_size.apply(int).sum() > max_total_size:
                b = c
            else:
                a = c
        return a

    def create_links(self, dir_name, already_sent=pd.DataFrame(), frozen_new=pd.DataFrame(), symbolic=True,
                     include_csv=True, include_release_notes=True, first_part=True, flat=False):
        """
        Create a directory tree with links to FITS files in archive

        Parameters
        ----------
        dir_name: str
            Directory name where links should be created
        already_sent: SpiceCat
            List of files of current release that have already been sent
        frozen_new: pandas.DataFrame
            New versions of the frozen files
        symbolic: bool
            Create symbolic links (otherwise create hard links)
        include_csv: bool
            Include a CSV export of the catalog
        include_release_notes: bool
            Include the release notes (hard link)
        first_part: bool
            These files are the first part of the release (then we need to
            initialize the directory for the release)
        flat: bool
            Create a flat directory structure (all links in same directory)
            instead of keeping the original structure
        """
        parent_dir = Path(dir_name)
        # create output directories
        if flat or self.empty:
            dir_names = [parent_dir]
        else:
            dir_names = self.apply(
                lambda row: parent_dir / get_file_relative_path(row),
                axis=1
            ).unique()
        for dir_name in dir_names:
            dir_name.mkdir(parents=True, exist_ok=True)
        # link files
        # link_to() and symlink_to() arguments are reversed, see pathlib doc,
        # so we define a function to consider both cases

        def link_func(row):
            source_file = get_file_fullname(row)
            if flat:
                target_file = parent_dir / row.FILENAME
            else:
                target_file = parent_dir / get_file_relative_path(row) / row.FILENAME
            if symbolic:
                return target_file.symlink_to(source_file)
            else:
                # TODO: from Python 3.10, link_to() is deprecated and replaced by hardlink_to(), with same argument order than symlink_to(); link_to() will be deprecated in Python 3.12
                return source_file.link_to(target_file)

        if not self.empty:
            self.apply(link_func, axis=1)
        if include_csv:
            if first_part:
                frozen_old = pd.DataFrame()
            else:
                frozen_old = self.get_previous_versions(frozen_new)
            release_files = pd.concat([already_sent, frozen_old, self]) \
                              .sort_values(['LEVEL', 'OBT_BEG'])
            print(f'{len(self)} new files, {len(already_sent)} files already in release, total {len(release_files)}')
            release_files.to_csv(parent_dir / "catalog.csv", index=False, date_format="%Y-%m-%dT%H:%M:%S.%f")
        if include_release_notes:
            filename = "release-notes.md"
            code_path = (Path(__file__) / ".." / "..").resolve()
            # Fill markdown template
            target_file = parent_dir / filename
            template_data = {
                'doi': config.doi,
                'release_tag': config.release_tag,
                'date': config.release_date,
            }
            jinja_env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(searchpath=code_path.as_posix()),
                autoescape=True
            )
            template = jinja_env.get_template(filename)
            filled_template = template.render(**template_data)
            with open(target_file, 'w') as f:
                f.write(filled_template)
            # Also convert to HTML and PDF
            formats = [
                ("html", ["--template", "./assets/bootstrap-template.html"]),
                ("pdf", ["--template", "./assets/eisvogel-template.tex", "--pdf-engine=xelatex"]),
            ]
            for to_format, extra_args in formats:
                target_file_converted = target_file.with_suffix("." + to_format)
                pypandoc.convert_file(
                    target_file.as_posix(),
                    format="markdown+yaml_metadata_block",
                    to=to_format,
                    outputfile=target_file_converted.as_posix(),
                    extra_args=extra_args
                )
            self._include_json_ld(target_file)

    def _include_json_ld(self, target_file):
        """
        Update HTML file to include dataset metadata as JSON-LD

        Parameters
        ----------
        target_file: Path
            Release notes files, without suffix
        """
        url = f'https://api.datacite.org/dois/application/vnd.schemaorg.ld+json/{config.doi}'
        response = requests.get(url)
        if response.ok:
            in_file = target_file.with_suffix(".html.sav")
            out_file = target_file.with_suffix(".html")
            assert out_file.exists()
            out_file.rename(in_file)
            doi_metadata = response.json()
            doi_metadata["description"] = doi_metadata["name"]   # see #18
            with open(in_file, "r") as f:
                lines = f.readlines()
            line_end_head = lines.index("</head>\n")
            with open(out_file, "w") as f:
                f.writelines(lines[:line_end_head])
                f.write('<script type="application/ld+json">\n')
                json.dump(doi_metadata, f, indent=2)
                f.write("\n</script>\n")
                f.writelines(lines[line_end_head:])
            in_file.unlink()
        else:
            print(f"[yellow]Warning[/]: DOI {config.doi} JSON-LD metadata not available")

    def publish_release(self, already_sent=pd.DataFrame(), frozen_new=pd.DataFrame(), first_part=True, sure=False,
                        dry_run=False, **kwargs):
        """
        Publish files to be released on the server.

        Assumes that files already sent, not present on disk, or with another
        version in the same release, have already been removed from list.
        Filtering out these files is typically done before sending to SOAR.

        Parameters
        ----------
        already_sent: SpiceCat
            Files in this release that have already been sent in this or
            a previous release
        frozen_new: pandas.DataFrame
            New versions of the frozen files
        first_part: bool
            These files are the first part of the release (then we need to
            initialize the directory for the release)
        sure: bool
            User is already sure to send these files (with no further
            user interaction)
        dry_run: bool
            Perform a dry run (no files are sent)
        """
        print(f'Preparing files to be published in data release {config.release_tag}')
        if self.empty:
            if already_sent.empty:
                print('[yellow]Warning:[/] No files provided or already sent')
                return
            print('[yellow]Warning:[/] No files provided; '
                  'still publishing release notes for already sent files')
        with TemporaryDirectory(prefix='tmp-spice-release-') as tmp_dir:
            print(f'Linking release files to {tmp_dir}')
            self.create_links(tmp_dir,
                              already_sent=already_sent,
                              frozen_new=frozen_new,
                              first_part=first_part,
            )
            if first_part:
                already_sent.create_links(
                    tmp_dir,
                    include_csv=False,
                    include_release_notes=False,
                    first_part=first_part,
                )
            if not has_enough_space(tmp_dir, config.release_server, config.release_path):
                print('[red]Cancelling transfer to data release server[/]')
                return
            cp_args = [
                'ssh',
                f'{config.release_server}',
                'cp',
                '-al',
                f'{config.release_path}/release-{config.previous_release_tag}',
                f'{config.release_path}/release-{config.release_tag}'
            ]
            rsync_args = [
                'rsync',
                '-avPL',
                '--chmod=Dgo+rx',
                f'{tmp_dir}/',
                f'{config.release_server}:{config.release_path}/release-{config.release_tag}/'
            ]
            if first_part:  # Delete outdated files from previous release
                rsync_args.insert(1, '--delete')
            if not sure:
                input_strings = [
                    'Will run commands',
                    f'{" ".join(rsync_args)}',
                    'Are you sure? [yN] '
                ]
                if dry_run:
                    input_strings[-1] += '(DRY RUN) '
                if first_part:
                    input_strings.insert(1, f'{" ".join(cp_args)}')
                confirm_send = input('\n'.join(input_strings))
                if not confirm_send.lower().startswith('y'):
                    print('[yellow]Warning:[/] User chose not to send files')
                    return
            if not dry_run:
                if first_part:  # Initialize from previous release directory
                    subprocess.run(cp_args, check=True)
                subprocess.run(rsync_args, check=True)
        if first_part:
            print(f'Updating metadata for latest release to {config.release_tag}')
            with TemporaryDirectory(prefix='tmp-spice-release-') as tmp_dir:
                with open(Path(tmp_dir) / 'latest-release.txt', 'w') as f:
                    f.write(f'{config.release_tag}\n')
                rsync_args = [
                    'rsync',
                    '-av',
                    '--chmod=Dgo+rx',
                    f'{tmp_dir}/',
                    f'{config.release_server}:{config.release_path}/metadata/'
                ]
                if not dry_run:
                    subprocess.run(rsync_args, check=True)
