import subprocess
import sys
from pathlib import Path
from tempfile import TemporaryDirectory
import pandas as pd
try:
    from rich import print
except ImportError:
    print('Warning: please install the Python `rich` module for colored output.')

from .spice_cat import SpiceCat, has_enough_space, human_size

try:
    from . import config
except ImportError:
    print('[red]Error:[/] Could not import config, please copy'
          'config-template.py to config.py and adapt it to your local setup')
    sys.exit(2)


class SoarSender:
    def __init__(self):
        self.sent_index_cols = ['release_tag', 'filename']
        self._read_list_sent_files()

    def _read_list_sent_files(self):
        if Path(config.list_sent_files).exists():
            print(f'Reading list of already-sent files from {config.list_sent_files}')
            self.sent = pd.read_csv(
                config.list_sent_files,
                index_col=self.sent_index_cols,
                dtype={'release_tag': str}
            )
            date_column = "sent_date"
            self.sent.loc[self.sent[date_column] == "MISSING", date_column] = "NaT"
            self.sent[date_column] = pd.to_datetime(self.sent[date_column], format="ISO8601")
        else:
            print('Creating new data structure for list of sent files')
            self.sent = pd.DataFrame({
                'release_tag': pd.Series(dtype=str),
                'filename': pd.Series(dtype=str),
                'level': pd.Series(dtype=str),
                'spiobsid': pd.Series(dtype=int),
                'rasterno': pd.Series(dtype=int),
                'version': pd.Series(dtype=int),
                'sent_date': pd.Series(dtype=object),
                'file_size': pd.Series(dtype=int),
            })
            self.sent.set_index(self.sent_index_cols, inplace=True)

    def _write_list_sent_files(self):
        self.sent.to_csv(config.list_sent_files)

    def _update_list_sent_files(self, spice_cat):
        """
        Add files to the list of sent files

        Parameters
        ----------
        spice_cat: SpiceCat
            Files to add to list of sent files

        Will raise a ValueError (from DataFrame.append()) if some files are
        already present in same release (these should be filtered out before
        calling this function)
        """
        now = pd.Timestamp.now(tz='UTC')
        newsent = pd.DataFrame({
            'release_tag': config.release_tag,
            'filename': spice_cat.FILENAME,
            'level': spice_cat.LEVEL,
            'spiobsid': spice_cat.SPIOBSID,
            'rasterno': spice_cat.RASTERNO,
            'version': spice_cat.VERSION,
            'sent_date': now.to_pydatetime().isoformat(timespec='seconds'),
            'file_size': spice_cat.file_size,
        })
        self.sent.reset_index(inplace=True)
        self.sent = pd.concat([self.sent, newsent])
        self.sent.drop_duplicates(self.sent_index_cols, inplace=True)
        self.sent.set_index(self.sent_index_cols, inplace=True, verify_integrity=True)

    def _is_reprocessed(self, cat_row):
        """
        Check whether catalog row corresponds to a file that has been
        reprocessed (same LEVEL, same SPIOBSID, same RASTERNO, different
        VERSION) and has already been sent in the same data release

        Parameters
        ----------
        cat_row: pandas.Series
            SPICE catalog entry

        Return
        ------
        bool
            True if file has an older version in the same data release.
        """
        same_level = (self.sent.level == cat_row.LEVEL)
        same_spiobsid = (self.sent.spiobsid == cat_row.SPIOBSID)
        same_rasterno = (self.sent.rasterno == cat_row.RASTERNO)
        other_version = (self.sent.version != cat_row.VERSION)
        same_release = (self.sent.index.get_level_values('release_tag') == config.release_tag)
        all_conditions = (same_level & same_spiobsid & same_rasterno & other_version & same_release)
        return all_conditions.any()

    def _exists_only_in_other_release(self, cat_row):
        """
        Check whether catalog row corresponds to a file that is not yet in
        current release, but that is already present in another release
        (same filename, different release_tag)

        Parameters
        ----------
        cat_row: pandas.Series
            SPICE catalog entry

        Return
        ------
        bool
            True if file is present only in different release(s)
        """
        same_release_index = (self.sent.index.get_level_values('release_tag') == config.release_tag)
        if cat_row.FILENAME in self.sent[same_release_index].index.get_level_values('filename'):
            return False  # File is already in current release
        elif cat_row.FILENAME in self.sent[~same_release_index].index.get_level_values('filename'):
            return True   # File is in different release(s)
        else:
            return False  # File is in no release yet

    def _filter_files(self, spice_cat: SpiceCat, freeze=False, total_size_limit=None):
        """
        Filter out files that should not be sent, keeping a list of already
        sent files

        Parameters
        ----------
        spice_cat: SpiceCat
            A list of files (as a partial SPICE catalog) from which already-sent
            files and other files not to send to SOAR have been filtered out
        freeze: bool
            Ignore new versions of files already in release
        total_size_limit: int
            Total size limit of all files being sent; next files will be ignored for this run

        Return
        ------
        to_send: SpiceCat
            Files to send
        already_sent: SpiceCat
            Files already sent
        frozen_new: pandas.DataFrame
            New versions of the frozen files
        """
        if spice_cat.empty:
            print('[yellow]Warning:[/] No files provided')
            return spice_cat, self.sent, pd.DataFrame()
        # Remove already-sent files
        where_sent = (spice_cat.FILENAME.isin(self.sent.index.get_level_values('filename')))
        already_sent = SpiceCat(spice_cat[where_sent])
        to_send = SpiceCat(spice_cat[~where_sent])
        if to_send.empty:
            print('[yellow]Warning:[/] All provided files have already been sent')
            return to_send, already_sent, SpiceCat()
        # Remove files not available on disk
        files_present_bool = to_send.check_file_presence()
        missing_files = SpiceCat(to_send[~files_present_bool])
        nmissing = len(missing_files)
        if nmissing:
            print(f'[yellow]Warning:[/] {nmissing} missing files (not existing on disk):')
            print('\n'.join(missing_files.list_file_names()))
        to_send = SpiceCat(to_send[files_present_bool])
        if to_send.empty:
            print('[yellow]Warning:[/] No file existing on disk can be sent')
            return to_send, already_sent, SpiceCat()
        # Detect files that already have another version in same release
        reprocessed_bool = to_send.apply(self._is_reprocessed, axis=1)
        if reprocessed_bool.any():
            if freeze:
                print(f'[yellow]Warning:[/] {reprocessed_bool.sum()} files have been reprocessed '
                      'but are frozen in the release with their older version')
                frozen_new = SpiceCat(to_send[reprocessed_bool])  # new versions of the frozen files
                to_send = SpiceCat(to_send[~reprocessed_bool])
            else:
                print('[red]Error:[/] Cancelling because of existence of reprocessed files '
                      'from files in same release [red](strengst verboten!)[/].')
                reprocessed = to_send[reprocessed_bool]
                print(f'{len(reprocessed)} files have a new version:')
                print(reprocessed[['FILENAME', 'SPIOBSID', 'RASTERNO', 'VERSION']])
                return SpiceCat(), SpiceCat(), SpiceCat()
        else:
            frozen_new = SpiceCat()
        # Limit total size of files being sent during this run
        if total_size_limit is not None:
            max_index = to_send.index_limit_size(total_size_limit)
            if max_index < len(to_send):
                print(f'[yellow]Warning:[/] Only {max_index}/{len(to_send)} files will be sent during this run because of {human_size(total_size_limit)}B size limit')
                to_send = SpiceCat(to_send.iloc[0:max_index])
        # Conclusion...
        print(f'{len(to_send)} files can be sent, total size: {human_size(to_send.total_size())}B')
        if not to_send.empty:
            print(to_send[['FILENAME', 'LEVEL', 'SPIOBSID', 'RASTERNO', 'XPOSURE', 'NWIN', 'SLIT_WID']])
        return to_send, already_sent, frozen_new

    def send_files(self, spice_cat: SpiceCat, sure=False, dry_run=False, freeze=False, total_size_limit=None, **kwargs):
        """
        Send all files in catalog to SOAR

        Parameters
        ----------
        spice_cat: SpiceCat
            A list of files (as a partial SPICE catalog)
        sure: bool
            User is already sure to send these files (with no further
            user interaction)
        dry_run: bool
            Perform a dry run (no files are sent, no change in local
            persistent file list)
        freeze: bool
            Ignore new versions of files already in release
        total_size_limit: int
            Total size limit of all files being sent; next files will be ignored for this run

        Return
        ------
        to_send: SpiceCat
            List of files that have really been sent
        already_sent: SpiceCat
            Files that have already been sent (among files in input list)
        frozen_new: pandas.DataFrame
            New versions of the frozen files
        """
        print('Preparing files to be sent to SOAR')
        # Filter out files that should not be sent
        to_send, already_sent, frozen_new = self._filter_files(
            spice_cat,
            freeze=freeze,
            total_size_limit=total_size_limit
        )
        if to_send.empty:
            return SpiceCat(), already_sent, frozen_new
        # Confirm that files should be sent
        if not sure:
            input_string = '\nAre you sure that you want to send these files to SOAR? [yN] '
            if dry_run:
                input_string += '(DRY RUN) '
            confirm_send = input(input_string)
            if not confirm_send.lower().startswith('y'):
                print('[yellow]Warning:[/] User chose not to send files')
                return SpiceCat(), already_sent, frozen_new
        # Send to GFTS server
        with TemporaryDirectory(prefix='tmp-spice-gfts-') as tmp_dir:
            print(f'Linking files to send by GFTS to {tmp_dir}')
            to_send.create_links(
                tmp_dir, flat=True,
                include_csv=False, include_release_notes=False
            )
            if not has_enough_space(tmp_dir, config.gfts_server, config.gfts_send_dir):
                print('[red]Cancelling transfer to SOAR[/]')
                return SpiceCat(), already_sent, frozen_new
            rsync_args = [
                'rsync',
                '-avPL',
                f'{tmp_dir}/',
                f'{config.gfts_server}:{config.gfts_send_dir}/'
            ]
            if not sure:
                input_strings = [
                    'Will run command',
                    f'{" ".join(rsync_args)}',
                    'Are you REALLY sure? [yN] '
                ]
                if dry_run:
                    input_strings[-1] += '(DRY RUN) '
                confirm_send = input('\n'.join(input_strings))
                if not confirm_send.lower().startswith('y'):
                    print('[yellow]Warning:[/] User chose not to send files')
                    return SpiceCat(), already_sent, frozen_new
            if not dry_run:
                subprocess.run(rsync_args, check=True)
        # Update and save self.sent
        if not dry_run:
            self._update_list_sent_files(already_sent)
            self._update_list_sent_files(to_send)
            self._write_list_sent_files()
        return to_send, already_sent, frozen_new
