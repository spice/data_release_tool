import pandas as pd
import matplotlib.pyplot as plt
try:
    from rich import print
except ImportError:
    print('Warning: please install the Python `rich` module for colored output.')

from .soar_sender import SoarSender
from .spice_cat import human_size

class Statistics:
    def __init__(self):
        self.sent = SoarSender().sent

    def get_release_tags(self):
        '''
        Get list of existing release tags.

        Return
        ------
        list
            List of release tags
        '''
        return list(self.sent.index.get_level_values('release_tag').unique())

    def select_release(self, tag):
        '''
        Select a release

        Parameters
        ----------
        tag: str
            Release tag

        Return
        ------
        pandas.DataFrame
            List of files in release
        '''
        return self.sent[self.sent.index.get_level_values('release_tag') == tag]
        
    def check_release(self, tag):
        '''
        Basic checks on a release

        Parameters
        ----------
        tag: str
            Release tag

        Return
        ------
        bool
            True if checks passed

        Current checks include: no duplicates. TODO: other checks.
        '''
        release = self.select_release(tag)
        # check for no duplicates in (LEVEL, SPIOBSID, RASTERNO)
        try:
            _ =  release.set_index(
                ['level', 'spiobsid', 'rasterno'],
                verify_integrity=True
            )
        except ValueError:
            return False
        else:
            return True
        
    def get_release_stats(self, human_units=False, human_names=False, human=False):
        '''
        Get statistics about data releases

        Return
        ------
        pandas.DataFrame
            Statistics: size (in bytes) and number of files for each release and each level.
        human_units: bool
            Output sizes in human-readable units
        human_names: bool
            Output column names for humans
        human: bool
            If True, this is a shortcut for both human_units and human_names being True
        '''
        if human:
            human_units = human_names = True
        releases = self.sent.drop(['spiobsid', 'rasterno', 'version'], axis=1)
        releases['number_files'] = 1
        stats = releases.groupby(['release_tag', 'level']).sum(numeric_only=True)
        stats = stats[['number_files', 'file_size']]  # reorder
        if human_units:
            stats['file_size_h'] = stats.apply(
                lambda row: f'{human_size(row.file_size)}B',
                axis=1
            )
            stats.drop('file_size', axis=1, inplace=True)
        if human_names:
            renames = {'number_files': 'Number of files'}
            if human_units:
                renames['file_size_h'] = 'Data volume'
            else:
                renames['file_size'] = 'Data volume (B)'
            stats.rename(renames, axis=1, inplace=True)
            stats.index.names = ['Release', 'Level']
        return stats

    def compare_releases(self, tag1, tag2):
        '''
        Compare 2 releases

        Parameters
        ----------
        tag1: str
            Release tag for 1st release
        tag2: str
            Release tag for 2nd release

        Return
        ------
        dict
            Lists of additions, deletions, and same files (with increasing, same, and decreasing version numbers)
        '''
        release1 = self.select_release(tag1)
        release2 = self.select_release(tag2)
        index_columns = ['level', 'spiobsid', 'rasterno']
        release1.set_index(index_columns, verify_integrity=True, inplace=True)
        release2.set_index(index_columns, verify_integrity=True, inplace=True)
        # Additions and deletions
        index1 = set(release1.index)
        index2 = set(release2.index)
        comp = dict()
        comp['additions'] = pd.DataFrame(
            sorted(list(index2 - index1)),
            columns=index_columns
        )
        comp['deletions'] = pd.DataFrame(
            sorted(list(index1 - index2)),
            columns=index_columns
        )
        # Compare version numbers for common data
        common = sorted(list(index1 & index2))
        release_common = release1.loc[common]
        release_common.rename(columns={'version': 'version1'}, inplace=True)
        release_common['version2'] = release2.loc[common]['version']
        def cmp(a, b):
            return (a > b) - (a < b) 
        version_comp = release_common.apply(
            lambda row: cmp(row['version1'], row['version2']),
            axis=1
        )
        colnames = ['common_version_increase', 'common_version_same', 'common_version_decrease']
        for colname, cmp_val in zip(colnames, [-1, 0, 1]):
            comp[colname] = pd.DataFrame(
                list(release_common.index[version_comp == cmp_val]),
                columns=index_columns
            )
        return comp

    def plot(self, axs=None, show=True, out_file=None):
        '''
        Plot statistics for data release

        Parameters
        ----------
        axs: iterable[matplotlib.axes.Axes]
            Two axes to be used for the plots
        show: bool
            Show plot
        out_file: str or Path
            File name to save plot to
        '''
        s = self.get_release_stats(human_names=True) \
            .rename({'Data volume (B)': 'Data volume (GB)'}, axis=1)
        s['Data volume (GB)'] /= 1e9
        levels = s.index.droplevel().unique()
        s_plot = {
            column: pd.DataFrame(
                {level: s[column].swaplevel().loc[level] for level in levels[::-1]}
            ) for column in s.columns
        }
        if axs is None:
            fig, axs = plt.subplots(1, 2, figsize=(8,4))
        for ax, column in zip(axs, s.columns):
            s_plot[column].plot.bar(stacked=True, ax=ax, title=column)
        plt.tight_layout()
        if out_file is not None:
            plt.savefig(out_file)
        if show:
            plt.show()
