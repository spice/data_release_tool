# Configuration for SPICE data release
# To be changed to your local settings and current release
# The given values are examples working on the IAS computing servers.

# Data where SPICE data files are stored
data_path = "/archive/SOLAR-ORBITER/SPICE"
# GFTS server and directory for files to send (no slash at end).
# Files sent to to_soc/ on GFTS server are then sent to SOC, but we don't
# send them directly there because `rsync` is not atomic.
# Username can be included with server name (user@server)
# or it can be configured in $HOME/.ssh/config
gfts_server = "solsoc@spice-gfts.ias.u-psud.fr"
gfts_send_dir = "/home/solsoc/to_soc_temporary_files"
# File storing list of files already sent to SOC
list_sent_files = "/home/ebuchlin/data_release_tool/list_sent_files.csv"
# Storage for releases: server and base directory
release_server = 'inf-drupal'
release_path = '/var/www/spice-data'
release_base_url = 'https://spice.osups.universite-paris-saclay.fr/spice-data/'
# Information for current (and previous) release
# (to be changed for each data release)
release_tag = '3.0'
previous_release_tag = '1.0'
release_date = '2022-03-08'
doi = f'10.48326/idoc.medoc.spice.{release_tag}'
