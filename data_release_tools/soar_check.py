import requests
import pandas as pd
try:
    from rich import print
except ImportError:
    print('Warning: please install the Python `rich` module for colored output.')

from .soar_sender import SoarSender


def format_date_range(date_range):
    """
    Return string for date range
    """
    assert len(date_range) == 2
    if date_range[0] is None:
        if date_range[1] is None:
            return ""
        else:
            return f"before {date_range[1].isoformat()}"
    else:
        if date_range[1] is None:
            return f"after {date_range[0].isoformat()}"
        else:
            return f"between {date_range[0].isoformat()} and {date_range[1].isoformat()}"


class SoarCheck:
    def __init__(self):
        self.tap_end_point = 'https://soar.esac.esa.int/soar-sl-tap/tap/'
        self.sent = SoarSender().sent
        # deduplicate filenames: keep only first occurrence
        self.sent = self.sent[~self.sent.index.get_level_values('filename').duplicated()]

    def _soar_sync_query(self, adql_query, print_query=False):
        """
        SOAR synchronous query

        Parameters
        ----------
        adql_query: str
            ADQL query
        print_query: bool
            If True, print query details

        Return
        ------
        dict
            JSON query result
        """
        payload = {
            'REQUEST': 'doQuery',
            'LANG': 'ADQL',
            'FORMAT': 'json',
            'QUERY': adql_query
        }
        if print_query:
            print(adql_query)
        r = requests.get(f'{self.tap_end_point}sync', params=payload)
        r.raise_for_status()
        if print_query:
            print(r.url)
        return r.json()

    def _is_on_soar(self, filename, print_query=False):
        """
        Check whether a specific file is on SOAR

        Parameters
        ----------
        filename: str
            Complete SPICE file name
        print_query: bool
            If True, print query details

        Return
        ------
        bool
            True if file is on SOAR
        """
        adql_query = f"SELECT filename FROM v_sc_repository_file WHERE filename='{filename}'"
        r = self._soar_sync_query(adql_query, print_query=print_query)
        if not r['data']:
            return False
        assert r['data'][0][0] == filename
        return True

    def _get_soar_inserted(self, date_range=None, tables=None, print_query=False):
        """
        Get list of files inserted in SOAR between 2 dates

        Parameters
        ----------
        date_range: tuple of datetime or pandas.Timestamp
            Date range of file insertion in SOAR
        tables: list
            SOAR tables to be queried
        print_query: bool
            If True, print query details

        Return
        ------
        dict of list
            For each table, list of files found in this table
        """
        if tables is None:
            tables = ['v_sc_data_item', 'v_sc_repository_file']
        results = dict()
        for table in tables:
            where = list()
            if table.endswith('data_item'):
                date_column = 'insertion_time'
                where.append("instrument='SPICE'")
            else:
                date_column = 'modification_date'
            for operator, date in zip(['>=', '<='], date_range):
                if date is None:
                    continue
                else:
                    where.append(f"({date_column}{operator}'{date.isoformat(sep=' ')}')")
            adql_query = f"SELECT filename FROM {table}"
            if len(where) > 0:
                adql_query += " WHERE " + ' AND '.join(where)
            r = self._soar_sync_query(adql_query, print_query=print_query)
            assert 'data' in r
            results[table] = [f[0] for f in r['data']]
        return results

    def check_files_on_soar(self, date_range=None, get_missing=False, display_missing=False, print_query=False):
        """
        Check that files sent over some date range are present on SOAR

        Parameters
        ----------
        date_range: tuple of str or datetime or pandas.Timestamp
            Date range on which the files have been sent (default: last week)
        get_missing: bool
            Return list of missing files
        display_missing: bool
            Display the list of missing files
        print_query: bool
            If True, print query details

        The code looks in SOAR over an extended range of insertion dates,
        and compares the sets of files on both ends.

        Date strings are assumed to be UTC.

        `None` in date range means not to check for this date range boundary.

        Return
        ------
        list
            Sent files that were not found in SOAR
        """
        if date_range is None:
            now = pd.Timestamp.now(tz='Etc/UTC')
            date_range = (now - pd.Timedelta(days=7), None)
        else:
            assert len(date_range) == 2
            date_range = tuple(
                None if date is None else pd.Timestamp(date, tz='Etc/UTC')
                for date in date_range
        )
        # Select by date
        sent_date_range = self.sent[
            (True if date_range[0] is None else self.sent.sent_date >= date_range[0]) &
            (True if date_range[1] is None else self.sent.sent_date <= date_range[1])
        ]
        sent_files = set(sent_date_range.index.get_level_values('filename'))
        print(f'{len(sent_files)} files sent {format_date_range(date_range)}')
        if not sent_files:
            return
        # Get files list from SOAR tables (on extended date range) and compare
        date_ext_min = None if date_range[0] is None else date_range[0] - pd.Timedelta(days=1)
        date_ext_max = None if date_range[1] is None else date_range[1] + pd.Timedelta(days=4)
        soar_files_tables = self._get_soar_inserted((date_ext_min, date_ext_max), print_query=print_query)
        remaining_files = sent_files
        first_table = True
        for soar_table in soar_files_tables:
            if not remaining_files:
                continue
            soar_files = set(soar_files_tables[soar_table])
            intersection = (remaining_files & soar_files)
            more_text = "" if first_table else "more "
            print(f'{len(intersection)} {more_text}files found in SOAR table {soar_table}')
            remaining_files -= intersection
            first_table = False
        # Find remaining files on SOAR
        if remaining_files:
            print(f'Checking {len(remaining_files)} files in table of all SOAR files')
        for filename in remaining_files.copy():
            if self._is_on_soar(filename, print_query=print_query):
                remaining_files.remove(filename)
        if remaining_files:
            print(f'[red]{len(remaining_files)} files not found in SOAR[/]')
            remaining_files = sorted(list(remaining_files))
            if display_missing:
                print('\n'.join(remaining_files))
        else:
            print('[green]All files found in SOAR[/]')
        if get_missing:
            return remaining_files
