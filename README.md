# SPICE data release tool

Code to select data files for data releases.


## How to install

* The following assumes that Python 3 is the default on your system, otherwise please use `python3` instead of `python`.
* Get the source code:
    * Using HTTPS: `git clone https://git.ias.u-psud.fr/spice/data_release_tool.git`
    * Or using an SSH key on the IAS GitLab server: `git clone git@git.ias.u-psud.fr:spice/data_release_tool.git`
* Optional: create and activate a new environment: `python -m venv .venv && . .venv/bin/activate`
* Install the requirements: `python -m pip install -r requirements.txt`
* In `data_release_tools/`, copy `config-template.py` to `config.py`, and adjust the parameters: path of your local SPICE data mirror, output path for the release files, GFTS server and directory, and the file in which the list of already-sent files is to be stored.


## How to use

* If starting a new release, provide in `config.py`: the current release tag, the previous release tag, and the release date. The release tag has to be changed for each new release.
* In `select_data.py`, edit how the FITS files selection is done in `release_select_files()`, and the steps for getting the complete set of files in `__main__()`, where the behaviour can be controlled by the parameters:
    * For a dry run (no files actually transferred), use `dry_run = True`.
    * If starting a new release, use `first_part = True`. Warning: this should be used only once for the new release; if some files are common with previous releases, all should be included in the first run of the code for the new release, otherwise they will not appear as part of the new release on the release server.
    * If some files that are already in the release have been reprocessed and have new versions, and if you want to ignore these new versions (they can be part of a next release), use `freeze = True`.
    * If you want to limit the data volume transferred during a given run, provide this limit (in bytes) as `total_size_limit`.
* Run `./select_data.py` or `python select_data.py` (using Python 3).
* If this was a new release: do not forget to revert to `first_part = False` and `freeze = False`.

Then the script:

* Prepares the files to be sent to SOAR, as symbolic links in a temporary directory
* Filters out files that have already been sent to SOAR, or files that are missing on disk
* Checks that the files can be added to the current release (no reprocessed file with more than one version in same release)
* Sends them to the GFTS server directory (from where they will be sent to SOAR)
    * Before that, the user is prompted for confirmation
    * On the GFTS server, as set in `config.py` of this script, the files should be transferred into a temporary directory and not directly into the GFTS output directory (typically `to_soar/`), because the file transfer is a non-atomic operation and we do not want the ESA client to transfer incomplete files. It is then up to the user to log in to the GFTS server and do an atomic move (typically `mv`) of the files from this temporary directory to the GFTS output directory.
    * The GFTS `transferred/` directory should regularly be cleaned (`rm transferred/*`) so that transferred files do not use too much space. For large transfers, the remaining space can be use as a guideline to set the limit to the data volume to be transferred during the next run of the script (`total_size_limit`).
* Updates the list of sent files
* Prepares the files to be sent to the release web server, as symbolic links in a temporary directory
    * This also includes the release notes (as Markdown, HTML and PDF), and a table of files included in the release (as CSV)
* Sends them to the release web server
    * Before that, the user is prompted for confirmation
    * The release web server contains one directory per release. Files common to several releases are hard links.

To check whether files have been successfully ingested and are available from SOAR:

```python
from data_release_tools import SoarCheck
s = SoarCheck()
s.check_files_on_soar(('2022-03-20', '2023-01-01'))  # choose date range
```


## Issues

See the [Gitlab issues](https://git.ias.u-psud.fr/spice/data_release_tool/-/issues).
