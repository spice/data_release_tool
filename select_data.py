#!/usr/bin/env python3

import pandas as pd

from data_release_tools import SpiceCat, SoarSender


def release_select_files(cat):
    """
    Select files in catalog, according to the criteria we define in this
    function (specific to each release)

    Parameters
    ----------
    cat: SpiceCat
        SPICE catalog
    """
    files = list()
    # Exclude SPIOBSIDs in "deny list"
    deny_list = pd.Series([], dtype=int)
    cat = cat[~cat.SPIOBSID.isin(deny_list)]
    # All L1 and L2 files since LTP100 to a recent date
    files.append(
        cat[
            (cat["DATE-BEG"] >= "2020-06-14T17:55") &
            (cat["DATE-BEG"] <  "2023-11-03") &
            ((cat.LEVEL == "L1") | (cat.LEVEL == "L2"))
        ]
    )
    ## DO NOT INCLUDE L3 FILES BEFORE UPDATING THE SOAR DESCRIPTORS LIST
    assert not any(pd.concat(files).LEVEL == "L3")
    return SpiceCat(pd.concat(files))


if __name__ == "__main__":
    params = {
        # is this a dry run? (no file transferred)
        'dry_run': False,
        # is this the first part of a new release?
        'first_part': False,
        # do we freeze existing file versions in release? (ignore new versions, keep them for next release)
        'freeze': False,
        # limit total size of files being sent during this run, in bytes (None: no limit)
        'total_size_limit': None,
    }
    # Select files
    print('Reading SPICE catalog')
    cat = SpiceCat(read_cat=True)
    print('Selecting data')
    selected_files = release_select_files(cat)
    selected_files.add_headers()
    # other_files = cat.find_file_dependencies(selected_files)
    all_files = SpiceCat(pd.concat([selected_files]))   # + other_files if needed
    # Send to GFTS server for SOAR
    s = SoarSender()
    sent_files, already_sent, frozen_new = s.send_files(all_files, **params)
    # Publish as data release
    sent_files.publish_release(already_sent=already_sent, frozen_new=frozen_new, **params)
