---
title: SPICE Data Release {{ release_tag }}
author: SPICE consortium
date: {{ date }}
geometry: a4paper
colorlinks: true
lang: en-US
standalone: true
titlepage: true
header-includes:
    <meta name="DC.Identifier" content="https://doi.org/{{ doi }}">
---

DOI:[{{ doi }}](https://doi.org/{{ doi }})

Access to data:

* For this release: [https://spice.osups.universite-paris-saclay.fr/spice-data/release-{{ release_tag }}/](https://spice.osups.universite-paris-saclay.fr/spice-data/release-{{ release_tag }}/).
* All SPICE data: through the ESA [Solar Orbiter Archive](http://soar.esac.esa.int/).
* All releases: [https://spice.osups.universite-paris-saclay.fr/spice-data/](https://spice.osups.universite-paris-saclay.fr/spice-data/)

This data pack contains Solar Orbiter/SPICE data recorded during:

* Cruise Phase: Short Term Planning periods (STP) 100 to 179. These data were acquired early in the mission, under variable instrument configurations.
* Nominal Mission Phase, from STP180.

We distribute both Level 2 (L2, calibrated) and Level 1 (L1, uncalibrated) files. However, scientists should use L2 and not L1 files for their research. L2 data take into account all the calibration parameters quantified at the time of the release. L1 files are only provided to investigate possible issues with the conversion from L1 to L2. The conversion software will eventually be released in SolarSoft. Users should contact the SPICE team in case they think they have a good reason to use L1 files.


## Citation and acknowledgements

Scientific papers using SPICE data from this data release must:

* Cite the SPICE instrument paper "SPICE consortium et al, A&A, 2020 DOI:[10.1051/0004-6361/201935574](https://doi.org/10.1051/0004-6361/201935574)";
* Cite this data release: DOI:{{ doi }}. This can be done following the suggestions by [DataCite](https://doi.datacite.org/dois/{{ doi | replace("/", "%2F") }});
* Include the following statement in the acknowledgements section:

> The development of SPICE has been funded by ESA member states and ESA. It was built and is operated by a multi-national consortium of research institutes supported by their respective funding agencies: STFC RAL (UKSA, hardware lead), IAS (CNES, operations lead), GSFC (NASA), MPS (DLR), PMOD/WRC (Swiss Space Office), SwRI (NASA), UiO (Norwegian Space Agency).

The usage of SPICE images as online web graphics or in printed materials must mention "Image Courtesy: ESA/Solar Orbiter/SPICE".


## Documentation

* The SPICE instrument is described in: [SPICE consortium et al 2020, A&A](https://doi.org/10.1051/0004-6361/201935574)
* The processing steps applied to produce the L2 data are described in section 3.3.3 of the [Data Product Description Document](https://spice-wiki.ias.u-psud.fr/lib/exe/fetch.php/public:spice-uio-dpdd-0002-2.1-data_product_description_document_spicefits_.pdf).
* The [SPICE data user's manual](https://spice-wiki.ias.u-psud.fr/doku.php/data:data_analysis_manual) (preliminary version)
* IDL users: please make sure that you have an up-to-date SolarSoftWare distribution
* Python users: if you are using [sunraster](https://github.com/sunpy/sunraster/) to open the files, please make sure that your version is >=0.4.3


## Known limitations

* The spatial resolution is lower than measured pre-flight: 5.4" from ground tests, 6.7" in flight.
* The spectral resolution is lower than measured pre-flight: (a) 2" slit, SW channel: ground test – 4.7 pixels, flight – 7.8 pixels; (b) 2" slit, LW channel: ground test – 5.3 pixels, flight – 9.4 pixels.
* There is a systematic bias in measurements of Doppler velocities correlated to the intensity gradients — an effect qualitatively similar to what was reported in SoHO/CDS ([Haugan 1999](https://doi.org/10.1023/A:1005110421725)) and Hinode/EIS ([Young et al. 2012](https://doi.org/10.1088/0004-637X/744/1/14); [Warren et al. 2018](https://doi.org/10.3847/1538-4357/aaa9b8)), although with a larger magnitude. The source of this bias appears to be a combination of anisotropic PSFs (i.e., astigmatism) in both the telescope and spectrometer sections. An effort is on-going to model the effect and to devise corrective actions. As of today, we recommend not to interpret Doppler velocities in SPICE data without contacting the instrument team for advice. A deconvolution software is being developped ([Plowman et al. 2023](https://ui.adsabs.harvard.edu/abs/2022arXiv221116635P/abstract)), the method is currently under test prior to deployment.
* Some specific observations have been done during spacecraft "Wheel Off-Loading" (WOL) events for testing purposes, then the pointing was unstable. The correct pointing information is however present in the `WCSDVARR` FITS file HDUs and the data can be rectified if needed.
* Observations made during commissioning and cruise-phase were mostly of engineering type and are therefore not all suitable for scientific analysis. Caution is recommended when using these. STP134 for example is unfit for scientific analysis.
* Files built from incomplete telemetry (with header `COMPLETE='I'`) are also unfit for scientific analysis.


## Change log

### New in Data Release 5.0

* **L2, updated absolute calibration**: compared to the files from Data Release 4.0 the irradiances of Data Release 5.0 are a factor 1.613 times higher for 2" slit observations and 2.016 times higher for the 4", 6", and 30" slits.
* **L2, dark subtraction**: for observations around perihelion the dark subtraction is obtained through the use of combined darks. For other periods, the subtracted darks are files that have been cleaned for cosmic rays. If no cleaned dark is available, a regular untreated dark is subtracted, as in previous data releases.
* **L2, burn-in correction**: updated description of burn-in, 2024-08-28. Compared to the previous data release, the following lines have a modified burn-in definition:
    * SW: N IV 76.51 nm, Ne VIII 77.04 nm
    * LW: H Lyman γ 97.25 nm, C III 97.7 nm, H Lyman β 102.6 nm, O IV 103.9 nm
* **L2, time dependent detector response**: new response table, 2024-08-08.
* **L2, distortion correction**: new distortion correction matrix, 2024-07-04.
* **L2, wavelength scale**: new temperature dependent wavelength scale
* **L1 and L2, pointing**:
   * The spacecraft pointing is now corrected for the stellar aberration effect. This is what is usually done on solar missions (the origin of helioprojective coordinates is at *apparent* Sun center), although it is inconsistent with the current [SO Metadata standard](https://s2e2.cosmos.esa.int/confluence/display/SOSP/Metadata+Definition+for+Solar+Orbiter+Science+Data).
   * Modified SPICE vs spacecraft offset correction.
* **L1 and L2, bug fix**: a bug that caused incorrect intensities for spectrally binned windows that had an on-board pixel level offset has been fixed.
* **L2, removed engineering files**: L1 files resulting from an engineering study to set-up detectors for full frame read-out (`STUDY_ID = 57`) do not contain any science data. These files are therefore no longer calibrated, meaning that 1704 L2 files of Data Release 4.0 have no counterpart in this data release.


### New in Data Release 4.0

* L2: The data have been corrected in some wavelength regions where the detector sensitivity has decreased due to burn-in by strong emission lines:
    * SW: N IV 76.5 nm, Ne VIII 77.0 nm, Ne VIII 78.0 nm
    * LW: C III 97.7 nm, H Lyman β 102.6 nm, O VI 103.2 nm
* L2: The data have been corrected for the time-dependent degradation of the detector sensitivity.
* L2: Binary table extensions containing detailed information on approximated and lost image planes due to lost telemetry have been removed.
* L1 and L2: Several changes have been made to the metadata contents. See the Data Product Description Document for details.
* Bugs that have been fixed:
  * L1 and L2: Rasters: `CRVAL1` values was shifted to the east by approximately one slit width.
  * L1 and L2: All study types: Roll angle and `CRVALi` were set to mean of the values of all exposures instead of the values at `CRPRIXj`.
  * L1 and L2: Distortion correction arrays were multiplied by `-1`
  * L2: Some files produced after February 2023 had missing pixels not being `NaN`.

### New in Data Release 3.0

* L1 and L2: Wide-slit observations are no longer reversed in the wavelength dispersion dimension, i.e. Instrument-X is now increasing with increasing wavelength pixel.
* L1 and L2: Temperature dependent SPICE vs spacecraft pointing offset taken into account when calculating WCS keywords. <!-- TODO reference to TN on pointing -->
* L1 and L2: Information on pointing variations due to spacecraft pointing instabilities during the observation is put in two separate `WCSDVARR` image extensions (one per dimension).
* L2: Adjacent windows that are merged now occur only once in the FITS file.
* L2: Binary table extensions containing detailed information on lost telemetry have been removed.
* L1 and L2: Several changes to the metadata contents. See the [Data Product Description Document](https://spice-wiki.ias.u-psud.fr/lib/exe/fetch.php/documents_public:spice-uio-dpdd-0002-1.7-data_product_description_document_spicefits_.pdf) for details.
